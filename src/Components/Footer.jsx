import React, { Component } from 'react'
import { Container, Col, Row} from 'react-bootstrap'


export default class Footer extends Component {
    render() {
        return (
            <Container className="mt-5">
                <Col></Col>
                <Col>Copyright 2020 Julián Bermolen</Col>
                <Col></Col>
            </Container>
        )
    }
}
