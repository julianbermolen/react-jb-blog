import React, { Component, state } from 'react';
import { Col, Row} from 'react-bootstrap'
import './Component.css'

class Post extends Component {
    constructor(props) {
        super(props);

        this.state = {
            post: 0
        }

    }


    render() {
        return (
            <div>
            {this.props.task.map(t =>
            <a onClick={() => { this.setState({ post: t.key })}}>
                <Row key={t.id}>
                    <Col sm={8}>
                        <h2><a href=''>{t.tittle}</a></h2>
                        <p>{t.description}</p>
                    </Col>
                    <Col sm={4}>
                        <img src={t.image} className="w-100"/>
                    </Col>
                </Row>
                </a>
                    )}
            </div>
        );
    }
}

export default Post;
