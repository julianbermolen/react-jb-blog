import React, { Component } from 'react'
import { Container, Row, Col, Image} from 'react-bootstrap'
import perfil from './images/perfil.png'
import { FaTwitter, FaFacebook,FaGitlab, FaLinkedin} from 'react-icons/fa'
import {IoIosAddCircleOutline} from 'react-icons/io'
import './Component.css'

export default class Presentation extends Component {
    render() {
        return (
            <Container className="min-height">
                <Row>
                    <Col>
                       <Image src={perfil} roundedCircle className="perfil-foto" />
                    </Col>
                    <Col xs={6} className="mt-4">
                        <h2>Julián Matias Bermolen</h2>
                        <p>System Architect - Full-Stack Developer - Analyst</p>
                        <p>
                            <a href="" className="mr-3"><FaFacebook size="2em" color="#3b5958" /></a>
                            <a href="" className="mr-3"><FaTwitter size="2em"/></a>
                            <a href="" className="mr-3"><FaGitlab size="2em" color="#e24329"/></a>
                            <a href="" className="mr-3"><FaLinkedin size="2em" color="#0e76a8"/></a>
                        </p>
                    </Col>
                    <Col>
                        <div className="mt-4">
                        <p><IoIosAddCircleOutline size="2em"/> Seguime! </p>
                        </div>
                    </Col>
                </Row>
            </Container>
        )
    }
}
