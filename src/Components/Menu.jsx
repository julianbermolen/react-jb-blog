import React, { Component } from 'react'
import { Navbar, Nav, NavDropdown, Form, FormControl, Button} from 'react-bootstrap'
import './Component.css'


export default class Menu extends Component {
    render() {
        return (<div>
            <Navbar bg="light" expand="lg">
                <Navbar>
                    <Navbar.Brand href="#home">
                    <p className="font-tittle">Julián Bermolen</p>
                    </Navbar.Brand>
                </Navbar>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                    <Nav.Link href="#home">Inicio</Nav.Link>
                    <Nav.Link href="#link">Publicaciones</Nav.Link>
                    <NavDropdown title="Categorias" id="basic-nav-dropdown">
                        <NavDropdown.Item href="#action/3.1">Gestión</NavDropdown.Item>
                        <NavDropdown.Item href="#action/3.2">Programación</NavDropdown.Item>
                        <NavDropdown.Item href="#action/3.3">DevOps</NavDropdown.Item>
                    </NavDropdown>
                    <Nav.Link href="#link">Contacto</Nav.Link>
                    </Nav>
                    <Form inline>
                        <FormControl type="text" placeholder="Quiero saber sobre ..." className="mr-sm-2" />
                        <Button variant="dark">Buscar</Button>
                    </Form>
                </Navbar.Collapse>
            </Navbar>
            </div>
        )
    }
}
