import React, { Component } from 'react';
import { Container, Row, Col, InputGroup, FormControl, Button} from 'react-bootstrap'
import './Component.css'

class Newsletter extends Component {
    render() {
        return (
            <Container fluid className="newsletter" >
            <Container >
                <Row>
                    <Col sm={4}><p className="font text-justify">Suscribite y no <br/>te pierdas nunca mis Post!</p></Col>
                    <Col sm={6}>
                        <InputGroup className="mb-3">
                            <FormControl
                            placeholder="☺ Dirección de Email"
                            aria-label="Recipient's username"
                            aria-describedby="basic-addon2"
                            />
                            <InputGroup.Append>
                            <Button variant="outline-secondary">Sucribirme</Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Col>
                    <Col sm={4}>
                    </Col>
                    
                </Row>
            </Container>
            </Container>
        );
    }
}

export default Newsletter;
