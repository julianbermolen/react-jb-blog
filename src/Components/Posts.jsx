import React, { Component } from 'react'
import { Container} from 'react-bootstrap'
import './Component.css'
import Post from './Post.jsx'
import task from './post.json'

export default class Posts extends Component {

    render() {
        return (
            <Container className="mt-5">
                <Post task={task}/>
            </Container>        
            )
    }
}
