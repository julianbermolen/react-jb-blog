import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import './App.css';
import Login from './Pages/Login/Login'
import Logout from './Pages/Logout/Logout'
import Home from './Pages/Home/Home'

export const STATE_LOGGED_IN = 'Logueado';
export const STATE_LOGGED_OUT = 'no-logueado';
export default class App extends Component{
  constructor(){
    super();



    this.state = {
      loggedIn: STATE_LOGGED_OUT,
      user:{},
    };
    this.loggedIn = this.loggedIn.bind(this);  

  }


componentDidMount(){
    this.isLoggedin();
  }
  

handleLogin = (data) => {
  this.setState({
    loggedIn: STATE_LOGGED_IN,
    user: data
  });
};

handleLogout = () => {
    this.setState({
      loggedIn: STATE_LOGGED_OUT,
      user: {}
    });
  };

  isLoggedin(){
    const token = localStorage.getItem("authToken");
    if(this.state.loggedIn === STATE_LOGGED_OUT && token){
      this.setState({
        loggedIn: STATE_LOGGED_IN,
        user: token
      });
    }
  }

  loggedIn = (data) => {
    if(data === STATE_LOGGED_IN){
      window.location = "/home"
    };
  };

componentDidUpdate(){
  if(this.state.loggedIn === STATE_LOGGED_IN && window.location.pathname === "/" ){
  this.loggedIn(this.state.loggedIn);
  }
}

  render() {
      return (
        <Router>
            <Route exact path="/">
              <Login handleLogin={this.handleLogin} loggedIn={this.state.loggedIn} />
            </Route>
            <Route exact path="/logout">
              <Logout handleLogout={this.handleLogout} />
            </Route>
            <Route exact path="/home">
              <Home loggedIn={this.state.loggedIn} />
            </Route>
      </Router>
      );
  }
}