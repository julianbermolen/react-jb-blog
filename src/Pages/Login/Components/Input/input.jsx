import React from 'react';
import './input.css'

const Input = ({attribute, handleChange, params}) => {
    return (
        <div> 
            <input 
                id={attribute.id}
                name={attribute.name}
                placeholder={attribute.placeholder}
                type={attribute.type}
                onChange={(e) => handleChange(e.target.name, e.target.value)}
                className={attribute.className}
            />
        </div>
    )
};

export default Input;