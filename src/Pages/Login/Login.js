import React, { Component } from 'react'
import {Redirect} from 'react-router'
import Input from './Components/Input/input'
import Label from './Components/Label/Label'
import 'bootstrap/dist/css/bootstrap.min.css';
import './Login.css'
import {Container, Row, Col, Button} from 'react-bootstrap'
import { STATE_LOGGED_IN } from '../../App';

export default class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user:' ',
            password:' '
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSuccessfullAuth = this.handleSuccessfullAuth.bind(this);
    }

    handleChange(name, value) {
        if(name==='username'){
            this.setState({user:value});
        } else {
            this.setState({password:value});
        }
    }

    async handleSubmit(){

            const requestOptions = {
                    method: 'POST',
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify({ 
                        email: this.state.user,
                        password:this.state.password
                    })
            };
            
            const response = await fetch('http://localhost:4000/login', requestOptions);
            console.log(response.ok);
            if(response.ok){
                const res = await response.json();
                localStorage.setItem("authToken", res.token)
                this.handleSuccessfullAuth(res);
            } else {
                console.log("token", localStorage.getItem("authToken"))
            }
             
        }

        handleSuccessfullAuth(data){
            this.props.handleLogin(data);
            this.props.history.push("/home");
        }
    
        componentDidMount(){
            console.log(this.props.loggedIn)
        }

    render(){

        if (this.props.loggedIn === STATE_LOGGED_IN){
            return <Redirect path="/" />
        }
        return (       
            <div id="login-background">
                <Container>
                    <Row>
                        <Col xs={6}>
                            <Col xs={8}  className="back">
                                <h2 className="tittle-login">¡ Bienvenido !</h2>
                                <h2 className="tittle-login">{this.props.loggedIn}</h2>
                                <hr className="hr-design"></hr>
                            <Label text='Usuario'/>
                            <Input attribute= {{
                                id: 'username',
                                name:'username',
                                type:'text',
                                placeholder:'Ingrese su usuario ...',
                                className:'form-control'
                            }}
                            handleChange={this.handleChange}
                            />
                            <Label text='Contraseña'/>
                            <Input attribute= {{
                                id: 'password',
                                name:'password',
                                type:'password',
                                placeholder:'Ingrese su contraseña ...',
                                className:'form-control'
                            }}
                            handleChange={this.handleChange}
                            /><br></br>
                            <Button variant="primary" size="lg" block onClick={this.handleSubmit}>Iniciar Sesión</Button>{' '}
                            </Col>
                            <Col xs={4}>

                            </Col>
                                
                        </Col>
                        <Col>
                        
                        </Col>
                            
                    </Row>
                </Container>
            </div>
        )
    }
}

