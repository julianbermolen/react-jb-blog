import React, { Component } from 'react'
import Footer from '../../Components/Footer';
import Menu from '../../Components/Menu.jsx'
import Newsletter from '../../Components/Newsletter';
import Posts from '../../Components/Posts';
import Presentation from '../../Components/Presentation';


export default class Home extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
               <Menu/>
               <Presentation/>
               <Newsletter/>
               <Posts/>
               <Footer/>
            </div>
        )
    }
}
